$(document).on({
	ready: function () {
		var progress = $("#progress");
		var bar = $("#progress__bar");
		var statustxt = $("#progress__percent");
		var completed = "0";

		$("#mainForm").submit(function(e) {
			$("#errors").html("");
			e.preventDefault();

			var data = new FormData();

			$.each($("#mainForm :file").prop("files"), function(i, file) {
				data.append("file[]", file);
			});

			var url = $(this).prop("action");

			var beforeSubmit = function() {
				if (window.File && window.FileReader && window.FileList && window.Blob) {
					if (! $("#fileInput").val() ) {
						$("#errors").html("Aucun fichier à envoyer !");
						return false
					}

					var checkFiles = function (filename, tps) { //Fonction anonyme permettant de vérifier qu"un fichier correspond à une des extensions de fichier
						var bool = false;
						var extension = filename.replace(/^.*\./, "").toLowerCase();
						for ( var i in tps ) {
							if ( (extension === tps[i]) && (! bool) ) {
								bool = true;
							}
						}
						return bool;
					};

					var sizeFiles = function (fls) {
						var size = 0;
						$.each(fls, function( k, v ) {
							size += v.size
						});
						return size;
					};

					var types = [
						"bmp",
						"gif",
						"jpg",
						"jpeg",
						"png",
						"ods",
						"ots",
						"xlr",
						"xls",
						"xlsx",
						"csv",
						"doc",
						"docx",
						"odm",
						"odt",
						"ott",
						"drw",
						"vsdx",
						"vsd",
						"odg",
						"pdf",
						"odp",
						"otp",
						"pps",
						"ppt",
						"pptx",
						"gz",
						"tar.gz",
						"tgz",
						"rar",
						"tar",
						"7z",
						"bz2",
						"tar.bz2",
						"zip",
						"text",
						"txt",
						"rtf"
					];

					var files = $("#fileInput").prop("files");

					//Vérification des extensions
					$.each(files, function(k, v) {
						if (! checkFiles(v.name, types)) {
							$("#errors").html("Le fichier " + v.name + " ne correspond à aucune extension autorisée");
							return false
						}
					});

					const KB = 1024;
					const MB = 1048576;
					const GB = 1073741824;
					const TB = 1099511627776;

					//Autorise les fichiers dont la taille totale est inférieure ou égale à 500MB
					var max = 500*MB;
					if (sizeFiles(files) > max) {
						$("#errors").html("La taille totale des fichiers dépasse les 500Mo autorisées");
						return false
					}

					$("#send").hide();
					$("#loading-send").show();
				} else {
					$("#errors").html("Veuillez mettre à jour votre navigateur pour pouvoir utiliser cette fonctionnalité !");
					return false;
				}
			}

			var onProgress = function(event, position, total, percentComplete) {
				//Barre de progression
				bar.width(completed + "%");
				statustxt.html(completed + "%");
			}

			var afterSuccess = function(msg) {
				$("#output").html(msg);
			}

			var req = $.ajax({
				url: url,
				type: "POST",
				data: data,
				async: false,
				cache: false,
				contentType: false,
				enctype: "multipart/form-data",
				processData: false,
				target: "#output",
				beforeSubmit: beforeSubmit(),
				uploadProgress: onProgress,
				success: afterSuccess,
				resetForm: true
			}, "json");
			$("#send").show();
			$("#loading-send").hide();
			return false;
		});
	}
});